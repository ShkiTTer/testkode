package com.example.testkode.interfaces

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}