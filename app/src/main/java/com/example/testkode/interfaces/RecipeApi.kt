package com.example.testkode.interfaces

import com.example.testkode.BuildConfig
import com.example.testkode.recipeModel.data.RecipeDetails
import com.example.testkode.recipeModel.data.RecipeDetailsResponce
import com.example.testkode.recipeModel.data.RecipeList
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface RecipeApi {
    companion object {
        fun create(): RecipeApi {
            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.RECIPES_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(RecipeApi::class.java)
        }
    }

    @GET("recipes")
    fun getRecipeList(): Observable<RecipeList>

    @GET("recipes/{uuid}")
    fun getRecipeDetails(@Path("uuid") uuid: String): Observable<RecipeDetailsResponce>
}