package com.example.testkode.recipeModel

import com.example.testkode.interfaces.RecipeApi
import com.example.testkode.recipeModel.data.Recipe
import com.example.testkode.recipeModel.data.RecipeDetails
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RecipeApiRepository(private val recipeApi: RecipeApi) {
    companion object {
        private var instance: RecipeApiRepository? = null

        fun getInstance(): RecipeApiRepository {
            if (instance == null) {
                instance = RecipeApiRepository(RecipeApi.create())
            }

            return instance!!
        }
    }

    private val compositeDisposable = CompositeDisposable()

    fun getRecipeList(showResult: (recipes: List<Recipe>) -> Unit, showError: () -> Unit) {
        val disposable = recipeApi.getRecipeList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { result ->
                    showResult(result.recipes)
                },
                { error ->
                    error.printStackTrace()
                    showError()
                })

        compositeDisposable.add(disposable)
    }

    fun getRecipeDetails(uuid: String, showResult: (recipes: RecipeDetails) -> Unit, showError: () -> Unit) {
        val disposable = recipeApi.getRecipeDetails(uuid)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { result ->
                    showResult(result.recipe)
                },
                { error ->
                    error.printStackTrace()
                    showError()
                })

        compositeDisposable.add(disposable)
    }
}