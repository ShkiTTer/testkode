package com.example.testkode.recipeModel.data

import android.os.Parcel
import android.os.Parcelable

data class Recipe(
    val uuid: String,
    val name: String,
    val images: List<String>,
    val lastUpdated: Int,
    val description: String?,
    val instructions: String,
    val difficulty: Int
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        listOf<String>().apply {
            parcel.readStringList(this)
        },
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()
    )

    companion object CREATOR : Parcelable.Creator<Recipe> {
        override fun newArray(size: Int): Array<Recipe?> {
            return arrayOfNulls(size)
        }

        override fun createFromParcel(source: Parcel?): Recipe {
            return Recipe(source!!)
        }
    }

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(uuid)
        dest?.writeString(name)
        dest?.writeStringList(images)
        dest?.writeInt(lastUpdated)
        dest?.writeString(description)
        dest?.writeString(instructions)
        dest?.writeInt(difficulty)
    }
}