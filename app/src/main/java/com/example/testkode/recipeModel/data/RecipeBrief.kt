package com.example.testkode.recipeModel.data

import android.os.Parcel
import android.os.Parcelable

data class RecipeBrief(
    val uuid: String,
    val name: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!
    )

    companion object CREATOR : Parcelable.Creator<RecipeBrief> {
        override fun createFromParcel(source: Parcel?): RecipeBrief = RecipeBrief(source!!)

        override fun newArray(size: Int): Array<RecipeBrief?> = arrayOfNulls(size)
    }

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(uuid)
        dest?.writeString(name)
    }
}