package com.example.testkode.recipeModel.data

import android.os.Parcel
import android.os.Parcelable

data class RecipeDetails(
    val uuid: String,
    val name: String,
    val images: List<String>,
    val lastUpdated: Int,
    val description: String?,
    val instructions: String,
    val difficulty: Int,
    val similar: List<RecipeBrief>
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.createStringArrayList()!!,
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.createTypedArrayList(RecipeBrief.CREATOR)!!.toList()
    )

    companion object CREATOR : Parcelable.Creator<RecipeDetails> {
        override fun createFromParcel(source: Parcel?): RecipeDetails = RecipeDetails(source!!)

        override fun newArray(size: Int): Array<RecipeDetails?> = arrayOfNulls(size)
    }

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(uuid)
        dest?.writeString(name)
        dest?.writeStringList(images)
        dest?.writeInt(lastUpdated)
        dest?.writeString(description)
        dest?.writeString(instructions)
        dest?.writeInt(difficulty)
        dest?.writeParcelableArray(similar.toTypedArray(), 0)
    }
}