package com.example.testkode.recipeModel.data

data class RecipeList(val recipes: List<Recipe>)