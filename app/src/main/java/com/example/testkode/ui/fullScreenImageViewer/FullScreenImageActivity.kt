package com.example.testkode.ui.fullScreenImageViewer

import android.Manifest
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.example.testkode.R
import com.example.testkode.utils.Constants
import com.example.testkode.utils.ImageHelper
import com.example.testkode.utils.PermissionManager
import com.github.chrisbanes.photoview.PhotoView
import kotlinx.android.synthetic.main.activity_full_screen_slider.*

class FullScreenImageActivity : AppCompatActivity() {
    companion object {
        private const val IMAGES_BUNDLE = "images_bundle"
        private const val START_INDEX_BUNDLE = "start_index_bundle"
        private const val RECIPE_NAME_BUNDLE = "recipe_name_bundle"
    }

    private val permissionManager = PermissionManager(this)

    private lateinit var images: List<String>
    private var startIndex = 0
    private var recipeName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_slider)
        setSupportActionBar(fullScreenToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        if (savedInstanceState != null) {
            images = savedInstanceState.getStringArray(IMAGES_BUNDLE)!!.toList()
            startIndex = savedInstanceState.getInt(START_INDEX_BUNDLE, 0)
            recipeName = savedInstanceState.getString(RECIPE_NAME_BUNDLE, "")
        } else {
            images = intent.getStringArrayExtra(Constants.FULLSCREEN_IMAGES_KEY)!!.toList()
            startIndex = intent.getIntExtra(Constants.FULLSCREEN_IMAGES_START_INDEX, 0)
            recipeName = intent.getStringExtra(Constants.FULLSCREEN_RECIPE_NAME)
        }

        setUpSlider()

        getPermission()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putStringArray(IMAGES_BUNDLE, images.toTypedArray())
        outState?.putInt(START_INDEX_BUNDLE, vpSliderFullscreen.currentItem)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_fullscreen, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            when (item.itemId) {
                android.R.id.home -> finish()
                R.id.saveImage ->
                    if (permissionManager.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ImageHelper.saveImage(this, "$recipeName ${vpSliderFullscreen.currentItem}.jpg", getCurrentImageUrl())
                    } else {
                        getPermission()
                    }
            }
        }

        return true
    }

    private fun getCurrentImageUrl(): String = images[vpSliderFullscreen.currentItem]

    private fun getPermission() {
        if (!permissionManager.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissionManager.requestPermissons(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))
        }
    }

    private fun setUpSlider() {
        val adapter = FullscreenSliderAdapter(this, images)

        vpSliderFullscreen.adapter = adapter
        tlSliderIndicatorFullscreen.setupWithViewPager(vpSliderFullscreen)

        vpSliderFullscreen.currentItem = startIndex
    }
}
