package com.example.testkode.ui.fullScreenImageViewer

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.testkode.R
import com.example.testkode.interfaces.OnItemClickListener
import com.example.testkode.utils.ImageHelper
import com.github.chrisbanes.photoview.PhotoView

class FullscreenSliderAdapter(private val context: Context, private val images: List<String>) : PagerAdapter() {
    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = images.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.image_slide_fullscreen, container, false)
        ImageHelper.loadImage(view as PhotoView, images[position])

        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }
}