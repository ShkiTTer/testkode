package com.example.testkode.ui.main

import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.testkode.R
import com.example.testkode.interfaces.OnItemClickListener
import com.example.testkode.recipeModel.RecipeApiRepository
import com.example.testkode.recipeModel.data.Recipe
import com.example.testkode.ui.recipeDetails.RecipeDetailsActivity
import com.example.testkode.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.design.indefiniteSnackbar
import org.jetbrains.anko.intentFor

class MainActivity : AppCompatActivity() {
    companion object {
        private const val RECIPES_BUNDLE = "recipes"
        private const val SEARCH_QUERY = "search_query"
        private const val SEARCH_ACTIVE = "search_active"
        private const val LAYOUT_MANAGER_STATE = "layout_manager_state"
    }

    private lateinit var recipeListAdapter: RecipeListAdapter
    private val recipeApiRepository = RecipeApiRepository.getInstance()
    private var recipes: List<Recipe> = listOf()

    private var isSearchOpen = false
    private var searchQuery = ""

    private val linearLayoutManager = LinearLayoutManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTitle(R.string.recipe_list_title)

        if (savedInstanceState == null) {
            recipeApiRepository.getRecipeList(this::setUpRecipeList, this::showNetworkError)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            recipes = savedInstanceState.getParcelableArrayList(RECIPES_BUNDLE)!!
            isSearchOpen = savedInstanceState.getBoolean(SEARCH_ACTIVE)
            searchQuery = savedInstanceState.getString(SEARCH_QUERY, "")

            val savedState: Parcelable? = savedInstanceState.getParcelable(LAYOUT_MANAGER_STATE)

            if (savedState != null) {
                linearLayoutManager.onRestoreInstanceState(savedState)
            }

            setUpRecipeList(recipes)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(RECIPES_BUNDLE, ArrayList(recipes))
        outState?.putBoolean(SEARCH_ACTIVE, isSearchOpen)
        outState?.putString(SEARCH_QUERY, searchQuery)
        outState?.putParcelable(LAYOUT_MANAGER_STATE, linearLayoutManager.onSaveInstanceState())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        if (menu != null) {
            val searchItem = menu.findItem(R.id.action_search)

            setUpSearchView(searchItem)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            when (item.itemId) {
                R.id.action_sort_by_name -> {
                    recipeListAdapter.sortByName()
                    item.isChecked = true
                }
                R.id.action_sort_by_date -> {
                    recipeListAdapter.sortByDate()
                    item.isChecked = true
                }
            }
        }

        return true
    }

    private fun setUpSearchView(searchItem: MenuItem) {
        val searchView = searchItem.actionView as SearchView

        searchView.maxWidth = Int.MAX_VALUE // Full width for search view

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String?): Boolean {
                if (query != null) {
                    searchQuery = query
                    recipeListAdapter.search(query)
                }

                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }
        })

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                isSearchOpen = false
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                isSearchOpen = true
                return true
            }
        })

        if (isSearchOpen) {
            val savedQuery = searchQuery
            searchItem.expandActionView()
            searchView.setQuery(savedQuery, false)
        }
    }

    private fun setUpRecipeList(recipes: List<Recipe>) {
        recipeListAdapter = RecipeListAdapter(this, recipes.sortedBy { it.name })
        this@MainActivity.recipes = recipes

        recipeListAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val uuid = recipeListAdapter.getRecipe(position).uuid

                startActivity(
                    intentFor<RecipeDetailsActivity>(
                        Constants.UUID_KEY to uuid
                    )
                )
            }
        })

        rvRecipeList.adapter = recipeListAdapter
        rvRecipeList.layoutManager = linearLayoutManager
    }

    private fun showNetworkError() {
        mainContainer.indefiniteSnackbar(R.string.snackbar_network_error, R.string.snackbar_network_action) {
            recipeApiRepository.getRecipeList(this::setUpRecipeList, this::showNetworkError)
        }
    }
}
