package com.example.testkode.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testkode.R
import com.example.testkode.interfaces.OnItemClickListener
import com.example.testkode.recipeModel.data.Recipe
import com.example.testkode.utils.ImageHelper
import kotlinx.android.synthetic.main.item_recipe_list.view.*

class RecipeListAdapter(private val context: Context, private var recipes: List<Recipe>) :
    RecyclerView.Adapter<RecipeListAdapter.ViewHolder>() {

    companion object {
        private var onItemClickListener: OnItemClickListener? = null
    }

    private enum class SortTypes {
        NAME, DATE
    }

    private val recipesCopy = recipes.toMutableList()
    private var lastSortType = SortTypes.NAME

    override fun getItemCount(): Int {
        return recipes.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_recipe_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvRecipeName.text = recipes[position].name
        holder.tvRecipeDescription.text =
            if (!recipes[position].description.isNullOrEmpty())
                recipes[position].description
            else
                context.getString(R.string.empty_description)

        val difficulty = recipes[position].difficulty

        when (difficulty) {
            in 1..2 -> holder.tvDifficulty.text = context.getString(R.string.difficulty_easy)
            in 3..4 -> holder.tvDifficulty.text = context.getString(R.string.difficulty_medium)
            else -> holder.tvDifficulty.text = context.getString(R.string.difficulty_hard)
        }

        ImageHelper.loadRoundImage(holder.ivRecipePhoto, recipes[position].images[0])
    }

    fun getRecipe(position: Int): Recipe {
        return recipes[position]
    }

    fun sortByName() {
        recipes = recipes.sortedBy { it.name }
        notifyDataSetChanged()
        lastSortType = SortTypes.NAME
    }

    fun sortByDate() {
        recipes = recipes.sortedBy { it.lastUpdated }
        notifyDataSetChanged()
        lastSortType = SortTypes.DATE
    }

    fun search(query: String) {
        recipes = recipesCopy.filter {
            it.name.toLowerCase().contains(query.toLowerCase())
                    || it.instructions.toLowerCase().contains(query.toLowerCase())
                    || (!it.description.isNullOrEmpty() && it.description.toLowerCase().contains(query.toLowerCase()))
        }

        if (lastSortType == SortTypes.NAME) {
            sortByName()
        } else sortByDate()
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val ivRecipePhoto = view.ivRecipeCardPhoto!!
        val tvRecipeName = view.tvRecipeCardName!!
        val tvRecipeDescription = view.tvRecipeCardDescription!!
        val tvDifficulty = view.tvDifficulty!!

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (onItemClickListener != null) {
                onItemClickListener!!.onItemClick(v!!, adapterPosition)
            }
        }
    }
}