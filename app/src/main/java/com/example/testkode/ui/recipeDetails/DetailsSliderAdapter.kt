package com.example.testkode.ui.recipeDetails

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.testkode.R
import com.example.testkode.interfaces.OnItemClickListener
import com.example.testkode.utils.ImageHelper

class DetailsSliderAdapter(private val context: Context, private val images: List<String>): PagerAdapter() {
    private var onItemClickListener: OnItemClickListener? = null

    override fun getCount(): Int = images.size

    override fun isViewFromObject(view: View, obj: Any): Boolean  = view == obj

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.image_slide_details, container, false)

        ImageHelper.loadImage(view as ImageView, images[position])

        view.setOnClickListener {
            if (onItemClickListener != null) {
                onItemClickListener?.onItemClick(view, position)
            }
        }

        container.addView(view)
        return view
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }
}