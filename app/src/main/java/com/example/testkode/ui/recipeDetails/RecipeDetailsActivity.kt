package com.example.testkode.ui.recipeDetails

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.example.testkode.R
import com.example.testkode.interfaces.OnItemClickListener
import com.example.testkode.recipeModel.RecipeApiRepository
import com.example.testkode.recipeModel.data.RecipeBrief
import com.example.testkode.recipeModel.data.RecipeDetails
import com.example.testkode.ui.fullScreenImageViewer.FullScreenImageActivity
import com.example.testkode.ui.fullScreenImageViewer.FullscreenSliderAdapter
import com.example.testkode.utils.Constants
import kotlinx.android.synthetic.main.activity_recipe_details.*
import kotlinx.android.synthetic.main.content_recipe_details.*
import org.jetbrains.anko.design.indefiniteSnackbar
import org.jetbrains.anko.intentFor

class RecipeDetailsActivity : AppCompatActivity() {
    companion object {
        private const val RECIPE_DETAILS_KEY = "recipe_details"
    }

    private val recipeApiRepository = RecipeApiRepository.getInstance()
    private lateinit var similarRecipesAdapter: SimilarRecipesAdapter

    private lateinit var recipeDetails: RecipeDetails

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val uuid = intent.getStringExtra(Constants.UUID_KEY)

        if (savedInstanceState == null) {
            recipeApiRepository.getRecipeDetails(uuid, this::showDetails, this::showError)
        }

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            recipeDetails = savedInstanceState.getParcelable(RECIPE_DETAILS_KEY)!!
            showDetails(recipeDetails)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(RECIPE_DETAILS_KEY, recipeDetails)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }

        return true
    }

    private fun showDetails(details: RecipeDetails) {
        recipeDetails = details

        setUpViewPager(details.images)

        when (details.difficulty) {
            in 1..2 -> tvRecipeDifficulty.text = getString(R.string.difficulty_easy)
            in 3..4 -> tvRecipeDifficulty.text = getString(R.string.difficulty_medium)
            else -> tvRecipeDifficulty.text = getString(R.string.difficulty_hard)
        }

        tvRecipeName.text = details.name
        tvRecipeInstructions.text = details.instructions.replace("<br>", "\n")
        tvRecipeDescription.text =
            if (!details.description.isNullOrEmpty())
                details.description
            else
                getString(R.string.empty_description)

        setUpSimilarList(details.similar)
    }

    private fun setUpViewPager(images: List<String>) {
        val sliderAdapter = DetailsSliderAdapter(this, images)
        vpSliderRecipeDetails.adapter = sliderAdapter
        tlSliderIndicatorRecipeDetails.setupWithViewPager(vpSliderRecipeDetails)

        sliderAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                startActivity(
                    intentFor<FullScreenImageActivity>(
                        Constants.FULLSCREEN_IMAGES_KEY to images.toTypedArray(),
                        Constants.FULLSCREEN_IMAGES_START_INDEX to position,
                        Constants.FULLSCREEN_RECIPE_NAME to recipeDetails.name
                    )
                )
            }
        })
    }

    private fun setUpSimilarList(similar: List<RecipeBrief>) {
        similarRecipesAdapter = SimilarRecipesAdapter(this, similar)
        rvSimilarRecipes.adapter = similarRecipesAdapter
        rvSimilarRecipes.layoutManager = LinearLayoutManager(this)

        similarRecipesAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val uuid = similarRecipesAdapter.getItem(position).uuid

                startActivity(
                    intentFor<RecipeDetailsActivity>(
                        Constants.UUID_KEY to uuid
                    )
                )

                finish()
            }
        })
    }

    private fun showError() {
        recipeDetailsContainer.indefiniteSnackbar(R.string.snackbar_network_error, R.string.snackbar_network_action) {
            val uuid = intent.getStringExtra(Constants.UUID_KEY)
            recipeApiRepository.getRecipeDetails(uuid, this::showDetails, this::showError)
        }
    }
}
