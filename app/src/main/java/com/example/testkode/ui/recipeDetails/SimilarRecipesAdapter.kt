package com.example.testkode.ui.recipeDetails

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testkode.R
import com.example.testkode.interfaces.OnItemClickListener
import com.example.testkode.recipeModel.data.RecipeBrief
import kotlinx.android.synthetic.main.item_similar_list.view.*

class SimilarRecipesAdapter(private val context: Context, private val similarRecipes: List<RecipeBrief>) :
    RecyclerView.Adapter<SimilarRecipesAdapter.ViewHolder>() {

    companion object {
        private var onItemClickListener: OnItemClickListener? = null
    }

    override fun getItemCount(): Int {
        return similarRecipes.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_similar_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvSimilarRecipeName.text = similarRecipes[position].name
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    fun getItem(position: Int): RecipeBrief {
        return similarRecipes[position]
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val tvSimilarRecipeName = view.tvSimilarRecipeName!!

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (onItemClickListener != null) {
                onItemClickListener!!.onItemClick(v!!, adapterPosition)
            }
        }
    }
}