package com.example.testkode.utils

object Constants {
    const val UUID_KEY = "uuid"

    const val FULLSCREEN_IMAGES_KEY = "fullscreen_images"
    const val FULLSCREEN_IMAGES_START_INDEX = "fullscreen_images_start_index"
    const val FULLSCREEN_RECIPE_NAME = "fullscreen_recipe_name"

    const val WRITE_EXTERNAL_STORAGE_PERMISSION = 1
}