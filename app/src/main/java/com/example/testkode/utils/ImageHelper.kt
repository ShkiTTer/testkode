package com.example.testkode.utils

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.widget.ImageView
import com.bumptech.glide.Glide
import org.jetbrains.anko.doAsync
import java.io.File
import java.io.FileOutputStream

object ImageHelper {
    private fun saveBitmapToFile(fileName: String, image: Bitmap) {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName)
        val out = FileOutputStream(file)

        image.compress(Bitmap.CompressFormat.JPEG, 100, out)
        out.close()
    }

    fun loadImage(view: ImageView, url: String) {
        Glide.with(view.context)
            .load(url)
            .into(view)
    }

    fun loadRoundImage(view: ImageView, url: String) {
        Glide.with(view.context)
            .load(url)
            .circleCrop()
            .into(view)
    }

    fun saveImage(context: Context, fileName: String, url: String) {
        val futureTarget = Glide.with(context)
            .asBitmap()
            .load(url)
            .submit()

        doAsync {
            saveBitmapToFile(fileName, futureTarget.get())
        }
    }
}