package com.example.testkode.utils

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

class PermissionManager(private val activity: Activity) {
    fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermissons(permissions: Array<String>) {
        ActivityCompat.requestPermissions(activity, permissions, Constants.WRITE_EXTERNAL_STORAGE_PERMISSION)
    }
}